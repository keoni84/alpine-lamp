Docker - Alpine lamp server

#### Notes

This is a personal build customized for my own use

#### run a default container

```
$ docker run -p 80:80 --name alpine-lamp -v /localdir:/www -v /localdir:/var/lib/mysql -v /localdir:/var/log/apache2 -d keoni84/alpine-lamp
```
